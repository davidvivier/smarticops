<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Dvivier\Smarticops\Permission;
use Dvivier\Smarticops\User;

class PermissionsControllerTest extends TestCase
{
    use DatabaseMigrations;
    
    public function setup() {
        
        parent::setup();
        
        $this->seed();
        
        Auth::loginUsingId(1);
        
        Auth::user()->loadPermissions();
        
        $this->permission = new Permission();
        $this->permission->code = 'world.dominate';
        $this->permission->description = 'Dominate the world';
        $this->permission->save();
        
        $this->permission = new Permission();
        $this->permission->code = 'world.create';
        $this->permission->description = 'Create a world';
        $this->permission->save();
        

    }
    
    public function testIndex() {
        
        $this->visit('permissions')
              ->see('world.dominate');
        $this->assertResponseOk();
        
        Permission::withTrashed()->delete();
        $this->visit('permissions')
              ->see('No permissions found.')
              ->dontSee('world.dominate');
        
    }
    
    public function testCreate() {
        
    //  test with valid data
        $this->visit('permissions/create')
              ->see('Create a Permission')
              ->type('world.destroy', 'code')
              ->type('Destroy the world', 'description')
              ->press('Create')
              ->seePageIs('permissions')
              ->see('world.destroy')
              ->see('The permission world.destroy has been created.');
        
        $permission = Permission::find(4);
        $this->assertEquals('world.destroy', $permission->code);
        $this->assertEquals('Destroy the world', $permission->description);
        
    // test with field missing
        $this->visit('permissions/create')
              ->see('Create a Permission')
              ->type('Destroy the world', 'description')
              ->press('Create')
              ->seePageIs('permissions/create')
              ->see('The "code" field is required.');
              
    // code already existing -> refused
        $this->visit('permissions/create')
              ->see('Create a Permission')
              // this code already exists
              ->type('world.dominate', 'code')
              ->type('Dominate the world', 'description')
              ->press('Create')
              ->seePageIs('permissions/create')
              ->see('This code is already used for another permission. Please choose another one.');
              
    }
    
    public function testEdit() {
        
        // valid data
        $this->visit('permissions/edit/2')
              ->see('Edit a Permission')
              // data already existing
              ->see('world.dominate')
              ->see('Dominate the world')
              ->type('another description', 'description')
              ->press('Save')
              ->seePageIs('permissions')
              ->see('The permission world.dominate has been updated')
              ->see('another description')
              ->dontSee('Dominate the world');
        
        // code already exists
        $this->visit('permissions/edit/2')
              ->see('Edit a Permission')
              ->see('world.dominate')
              // code is used for another permission
              ->type('world.create', 'code')
              ->press('Save')
              ->seePageIs('permissions/edit/2')
              ->see('This code is already used for another permission. Please choose another one.');
              
        
    }
    
    public function testDelete() {
        
        $user = User::create();
        $permission = Permission::find(2);
        $user->givePermission($permission->code);
        $this->assertTrue($user->hasPermission($permission->code, true));
        
        // if the permission is associated with a user
        
        $this->visit('permissions/delete/2')
              ->seePageIs('permissions')
              ->see('This permission is associated with 1 user(s) and 0 role(s) : it cannot be deleted now.');
        
        // then, when the permission is not assigned any more...
        $user->removePermission($permission->code);
        
        //  ...it can now be deleted.
        $this->visit('permissions/delete/2')
              ->see('The permission world.dominate has been deleted.')
              // check soft delete
              ->seeInDatabase( 'permissions', [
                                        'code' => 'world.dominate',
                                        ])
              ->notSeeInDatabase( 'permissions', [
                                    'code' => 'world.dominate',
                                    'deleted_at' => null,
                                    ]);;
        

        
    }
}
