<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Dvivier\Smarticops\Permission;

class PermissionTest extends TestCase
{
    use DatabaseMigrations;
    
    public function testSoftDeletePermission() {
        
        $permission = new Permission;
        $permission->code = 'world.dominate';
        $permission->save();
        
        $this->seeInDatabase( 'permissions', [
                                                'id'         => $permission->id,
                                                'deleted_at' => null 
                                                ]);
        
        $permission->delete(); // soft delete !
        
        $this->seeInDatabase(       'permissions', [
                                                'id'         => $permission->id,
                                                ])
                ->notSeeInDatabase( 'permissions', [
                                                'id'         => $permission->id,
                                                'deleted_at' => null 
                                                ]);
    }
    
    
}
