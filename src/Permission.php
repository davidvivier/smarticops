<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */

namespace Dvivier\Smarticops;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Dvivier\Smarticops\Role;
use Dvivier\Smarticops\User;

/**
 * The permission model for the Smarticops features.
 * 
 * @author David Vivier
 */
class Permission extends Model
{
    use SoftDeletes;
    
    protected $dates = [ 'deleted_at' ];
    
    
    protected $guarded = array('*');
    
    protected $fillable = [
                            'code',
                            'description',
                            ];
                            
    public function users() {
        return $this->belongsToMany(User::class, 'users_permissions')->withPivot('value', 'created_by')->withTimeStamps();
    }
    
    public function roles() {
        return $this->belongsToMany(Role::class, 'roles_permissions')->withPivot('value', 'created_by')->withTimeStamps();
    }
    
}
