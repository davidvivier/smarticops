<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */
 
namespace Dvivier\Smarticops;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

use Illuminate\Foundation\Auth\ResetsPasswords;

use Config;

use Dvivier\Smarticops\Facades\Smarticops;

/**
 *  Uses and overrides some Illuminate\Foundation\Auth\ResetsPasswords methods
 * 
 *  Copying methods here allows not to change the code directly in Laravel sources
 * 
 * 
 * @author David Vivier
 */
trait SmarticopsResetsPasswords 
{
    use ResetsPasswords {
        ResetsPasswords::showResetForm as parent_showResetForm;
        //ResetsPasswords::getSendResetLinkEmailSuccessResponse as parent_getSendResetLinkEmailSuccessResponse;
    }
    
    
    /**
     * Send a reset link to the given user.
     * 
     * Method copied from ResetsPasswords and adapted.
     * The parent method is overrided and **not** called.
     * 
     * 
     * @param  \Illuminate\Http\Request  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $broker = $this->getBroker();
        
        $response = Password::broker($broker)->sendResetLink($request->only('email'), function (Message $message) {
            $message->subject(trans('smarticops::passwords.email.subject'));
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                Smarticops::logPasswordResetRequest($request);
                return $this->getSendResetLinkEmailSuccessResponse($response);
            case Password::INVALID_USER:
            default:
                Smarticops::logPasswordResetRequest($request, false);
                return $this->getSendResetLinkEmailFailureResponse($response);
        }
    }
    
    /*
    public function getSendResetLinkEmailSuccessResponse($response) {
        var_dump($response);
        return $this->parent_getSendResetLinkEmailSuccessResponse($resonse);
    }*/
    
    
    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     * 
     * Copied and adapted from the parent method which is overrided and not called.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Http\Response
     * 
     * @see Illuminate\Foundation\Auth\ResetsPasswords::showResetForm ResetsPasswords::showResetForm
     */
    public function showResetForm(Request $request, $token = null) {
        if (isset($token)) {
            // log only if the page is accessed via email with token,
            //  otherwise logging is not necessary
            Smarticops::logPasswordResetClicked($request, $token);
        }
        
        
        return $this->parent_showResetForm($request, $token);
        /*
        if (is_null($token)) {
            $this->linkRequestView = 'smarticops'
            return $this->getEmail();
        }

        $email = $request->input('email');

        if (property_exists($this, 'resetView')) {
            return view($this->resetView)->with(compact('token', 'email'));
        }

        if (view()->exists('auth.passwords.reset')) {
            return view('smarticops::auth.passwords.reset')->with(compact('token', 'email'));
        }

        return view('smarticops::auth.reset')->with(compact('token', 'email'));*/
    }

    
    
    /**
     * Reset the given user's password.
     *
     * Method copied from ResetsPasswords and adapted.
     * The parent method id overrided and **not** called.  
     * 
     * Since the content of this method is essentially copied from Laravel's core, it will not be updated directly.
     * That's the reason why this package is only compatible with Laravel `5.2.x` .  
     * For future versions of Laravel, it will be necessary to check that the code of the method `ResetsPasswords::sendResetLinkEmail()`has not changed, or if it has, update this method consequently.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        $this->validate($request, $this->getResetValidationRules());

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                Smarticops::logPasswordReset($request, true);
                return $this->getResetSuccessResponse($response);
            default:
                Smarticops::logPasswordReset($request, false);
                return $this->getResetFailureResponse($request, $response);
        }
    }
    
    
    /**
     * Get the password reset validation rules.
     *
     * Overrides parent method, but does **not** call it.
     * 
     * @return array
     * 
     * @see ResetsPasswords ResetsPasswords
     */
    protected function getResetValidationRules()
    {
        // must be the same rules than the one in AuthController, and then add token rule
        $rules = [
                    'token'     => 'required',
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:'.Config::get('smarticops.password.min',8).'|confirmed',
                ];
        $messages = [
                        'required'      => trans('smarticops::validation.required'),
                        'min'           => trans('smarticops::validation.custom.password.min'),
                        'confirmed'     => trans('smarticops::validation.custom.password.confirmed'),
                        'unique'        => trans('smarticops::validation.custom.email.unique'),
                        'max'           => trans('smarticops::validation.max.string'),
                        ];
        return Validator::make($data, $rules, $messages);
        /*
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:'.Config::get('smarticops.password.min',8),
        ];*/
    }
    
}
