<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */
    return [
            
            // default settings for the SuperAdmin.
            // You MUST change the default password !
            'superadmin' => [
                                'first_name'    => env('SUPERADMIN_FIRSTNAME', 'Jean'),
                                'last_name'     => env('SUPERADMIN_LASTNAME', 'Régis'),
                                'email'         => env('SUPERADMIN_EMAIL', 'admin@smartic.ca'),
                                'password'      => env('SUPERADMIN_PASSWORD', '123456789'),
                                ],
            
            // change this to match your application's name
            'application_name'  => env('APP_NAME'),
            
            // the path to the files where events related to the permissions will be logged.
            //  this path starts from <project>/storage
            // no trailing slash.
            'log_path' => 'logs/smarticops/smarticops',
            
            // number of minutes the confirmation token is valid
            'confirm_expiration' => 60,
            
            // the locales currently available for Smarticops package                    
            'locales_available' => array(
                                            'en',
                                            'fr',
                                            ),
            
            // password configuration
            'password' => [
                // minimum number of characters for passwords
                'min' => 8,
                ],
        ];
        