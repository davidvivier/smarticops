<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */

namespace Dvivier\Smarticops\Listeners;

use Dvivier\Smarticops\Facades\Smarticops;
use Auth;
use Session;

/**
 * Log activity for authentication events.
 */
class AuthEventListener
{
    
    public function attempting($event) {
        //var_dump($event);exit;
        Smarticops::logAttempt($event->credentials['email']);
    }
    
    public function login($event) {
        $user = $event->user;
        $remember = $event->remember;
        Smarticops::logLogin($user, $remember);
    }   
    
    public function logout($event) {
        //var_dump($event->user);exit;
        if (isset($event->user)) {
            Smarticops::logLogout($event->user);
        }
        Session::forget('permissions');
    }
    
    public function lockout($event) {
        Smarticops::logLockout($event->request->email);
    }
    
    public function subscribe($events) {
        $events->listen(
            'Illuminate\Auth\Events\Attempting',
            'Dvivier\Smarticops\Listeners\AuthEventListener@attempting'
        );
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'Dvivier\Smarticops\Listeners\AuthEventListener@login'
        );
        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'Dvivier\Smarticops\Listeners\AuthEventListener@logout'
        );
        $events->listen(
            'Illuminate\Auth\Events\Lockout',
            'Dvivier\Smarticops\Listeners\AuthEventListener@lockout'
        );
    }
}
