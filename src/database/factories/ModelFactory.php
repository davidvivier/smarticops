<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */
 

$factory->define(Dvivier\Smarticops\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});



$factory->define(Dvivier\Smarticops\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => str_random(7),
        'description' => str_random(30),
    ];
});

