<?php

use Illuminate\Database\Seeder;
//use Config;

class SmarticopsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // The 3 system records for the SuperAdmin.
        
        // user
        DB::table('users')->insert([
                                    'first_name'    => Config::get('smarticops.superadmin.first_name', 'Jean'),
                                    'last_name'     => Config::get('smarticops.superadmin.last_name', 'Régis'),
                                    'email'         => Config::get('smarticops.superadmin.email', 'admin@smartic.ca'),
                                    'password'      => bcrypt(Config::get('smarticops.superadmin.password', '123456789')),
                                    'uuid'          => self::uuidv4(),
                                    'confirmed'     => true,
                                    'created_at'    => date("Y-m-d H:i:s"),
                                    'updated_at'    => date("Y-m-d H:i:s"),
                                    ]);
        // permission                     
        DB::table('permissions')->insert([
                            'uuid'      => self::uuidv4(),
                            'code'      => 'sa',
                            'description' => 'access the administration panel',
                            ]);
        
        // pivot       
        DB::table('users_permissions')->insert([
                                        'uuid'  => self::uuidv4(),
                                        'user_id'   => 1,
                                        'permission_id' => 1,
                                        'value'     => true,
                                        ]);
    }
    
    /**
     * Generates a version 4 UUID.
     * 
     * @see  https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_.28random.29  Wikipedia UUID Article
     * @see  http://stackoverflow.com/a/2040279/6161127  Stackoverflow Answer
     * @see  http://php.net/manual/en/function.uniqid.php#94959  Comment on PHP manual
     */
    public static function uuidv4() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        
        // 32 bits for "time_low"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),
        
        // 16 bits for "time_mid"
        mt_rand(0, 0xffff),
        
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,
        
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,
        
        // 48 bits for "node"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}
