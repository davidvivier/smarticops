<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */
 
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            
            //$table->string('name');
            $table->string('email')->index();
            $table->string('password');
            
            $table->string('first_name');
            $table->string('last_name');
            
            
            $table->boolean('confirmed')->default(false);
            $table->string('confirm_token')->nullable()->default(null);
            $table->timestamp('confirm_created_at')->nullable()->default(null);
            
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            
            
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_by');

            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
    
}
