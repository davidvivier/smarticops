<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */

Route::group(['middleware' => ['web']], function () {
    
    //  vendor/laravel/framework/src/Illuminate/Routing/Router.php:367
    //Route::auth();
    
    //*** These routes replace Laravel's routes from Route::auth() 
    
        // Authentication Routes...
        Route::get('login', 'Dvivier\Smarticops\Http\Controllers\Auth\AuthController@showLoginForm');
        Route::post('login', 'Dvivier\Smarticops\Http\Controllers\Auth\AuthController@login');
        Route::get('logout', 'Dvivier\Smarticops\Http\Controllers\Auth\AuthController@logout');

        // Registration Routes...
        Route::get('register', 'Dvivier\Smarticops\Http\Controllers\Auth\AuthController@showRegistrationForm');
        Route::post('register', 'Dvivier\Smarticops\Http\Controllers\Auth\AuthController@register');

        // Password Reset Routes...
        
        Route::get('password/reset/{token?}', 'Dvivier\Smarticops\Http\Controllers\Auth\SmarticopsPasswordController@showResetForm');
        Route::post('password/email', 'Dvivier\Smarticops\Http\Controllers\Auth\SmarticopsPasswordController@sendResetLinkEmail');
        Route::post('password/reset', 'Dvivier\Smarticops\Http\Controllers\Auth\SmarticopsPasswordController@reset');
    
    //***
    
    
    Route::get('/sendtoken/{user}', 'Dvivier\Smarticops\Http\Controllers\ConfirmController@sendConfirmToken');
    
    Route::get('/confirm/{token}', 'Dvivier\Smarticops\Http\Controllers\ConfirmController@confirm');
    
    Route::get('/', 'Dvivier\Smarticops\Http\Controllers\PagesController@welcome');

    
    Route::get('/home', 'Dvivier\Smarticops\Http\Controllers\HomeController@index');
    
    Route::get('lang/{locale}', function($locale) {
        $available = Config::get('smarticops.locales_available', null);
        if (in_array($locale, $available)) {
            Session::put('locale', $locale);
        }
        else {
            // maybe an error message
        }
        return Redirect::back();
    });
    
    
    Route::group(['middleware' => ['auth', 'admin']], function() {
        // all routes below require that a user is logged in AND has the SuperAdmin permission.
        
        
        Route::get('/admin', 'Dvivier\Smarticops\Http\Controllers\PagesController@admin');
        
        
        // PERMISSIONS
        
        Route::get('/permissions', 'Dvivier\Smarticops\Http\Controllers\PermissionsController@index');
        
        Route::get('/permissions/create', 'Dvivier\Smarticops\Http\Controllers\PermissionsController@create');
        
        Route::get('/permissions/edit/{permission}', 'Dvivier\Smarticops\Http\Controllers\PermissionsController@edit');
        
        Route::post('/permissions/create', 'Dvivier\Smarticops\Http\Controllers\PermissionsController@create_post');
        
        Route::post('/permissions/edit/{permission}', 'Dvivier\Smarticops\Http\Controllers\PermissionsController@edit_post');
        
        Route::get('/permissions/delete/{permission}', 'Dvivier\Smarticops\Http\Controllers\PermissionsController@delete');
        
        //  USERS
        //Route::get('/users/', 'UsersController@index');
        Route::get('/users', 'Dvivier\Smarticops\Http\Controllers\UsersController@index');
    
        Route::get('/users/create', 'Dvivier\Smarticops\Http\Controllers\UsersController@create');
        
        //Route::get('/users/edit/', 'UsersController@index');
        Route::get('/users/edit/{user}', 'Dvivier\Smarticops\Http\Controllers\UsersController@edit');
        
        
        Route::post('/users/create', 'Dvivier\Smarticops\Http\Controllers\UsersController@create_post');
        
        Route::post('/users/edit/{user}', 'Dvivier\Smarticops\Http\Controllers\UsersController@edit_post');
        
        //Route::delete('/users/delete/{user}', 'UsersController@delete');
        Route::get('/users/delete/{user}', 'Dvivier\Smarticops\Http\Controllers\UsersController@delete');
        
        
        
        // ROLES
        
        Route::get('/roles', 'Dvivier\Smarticops\Http\Controllers\RolesController@index');
        
        Route::get('/roles/create', 'Dvivier\Smarticops\Http\Controllers\RolesController@create');
        
        Route::get('/roles/edit/{role}', 'Dvivier\Smarticops\Http\Controllers\RolesController@edit');
        
        Route::post('/roles/create', 'Dvivier\Smarticops\Http\Controllers\RolesController@create_post');
        
        Route::post('/roles/edit/{role}', 'Dvivier\Smarticops\Http\Controllers\RolesController@edit_post');
        
    
        Route::get('/roles/delete/{role}', 'Dvivier\Smarticops\Http\Controllers\RolesController@delete');    
    });
});
