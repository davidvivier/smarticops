<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */

namespace Dvivier\Smarticops\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use Session;

/**
 * This middleware will check if the logged user is an administrator
 *  and return an error if not
 * 
 * @author David Vivier
 */
class SmarticopsAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //var_dump(Auth::user());exit;
        //var_dump(Auth::user()->permissions->all());exit;
        //var_dump(Session::get('func', 'nofunc'));
        
        //var_dump(Session::get('permissions')->last()->code);
        //var_dump(Auth::user()->hasPermission('sa', true));
        //var_dump(Session::get('permissions'));
        //return $next($request);
        if (Auth::user()->hasPermission('sa')) {
            // superadmin
            //var_dump('yes');
            //Session::flash('errors', 'ksrghjsdlkfrhgh !');
            
            return $next($request);
        }
        else {
            //var_dump(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 5));
            /*
            $d = array();
            foreach(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 7) as $e) {
                if (isset($e['file'])) {
                    $d[] = $e['file'];
                }
                else {
                    $d[] = $e['function'];
                }
            }
            */
            //var_dump($d);
            //var_dump('no');
            //return redirect()->back()->withErrors([trans('smarticops::permissions.error.not_allowed')]);
            return redirect('/')->withErrors([trans('smarticops::permissions.error.not_allowed')]);
        }
        
        return $next($request);
    }
}
