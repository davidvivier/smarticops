<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */
 
namespace Dvivier\Smarticops\Http\Controllers;

use Illuminate\Http\Request;

use Dvivier\Smarticops\Http\Requests;

use Dvivier\Smarticops\User;
use Mail;
use Auth;
use Session;
use Config;


/**
 *  A controller to manage the email confirmation feature.
 * 
 */
class ConfirmController extends Controller {
    
    /**
     * Creates a confirmation token and sends email to the user
     *  with a confirmation link
     * 
     * @param  Request  $request
     * @param  User  $user
     * 
     * @author David Vivier
     */
    protected function sendConfirmToken(Request $request, User $user) {
        
        if (Auth::check()) {
            if (Auth::user()->id != $user->id) {
                return redirect()->back()->withErrors(['Error']);
            }
        }
        
        $token = str_random(64);
        $user->confirm_token = $token;
        $user->confirm_created_at = date("Y-m-d H:i:s");
        $user->save();
        
        $confirmURL = url('/confirm/'.$token);
        
        $res = Mail::send('smarticops::auth.emails.confirm', [  
                                                    'user'  => $user,
                                                    'url' => $confirmURL,
                                                    ], 
                                                function ($m) use ($user) {
                                                    //$m->to('d.vivier@smartic.ca');
                                                    $m->to($user->email);
                                                    //$m->from('');
                                                    $m->subject(trans('smarticops::auth.confirm.subject'));
                                                });
        //var_dump($res);exit;
        //return redirect->back();
        
        Session::flash('info', trans('smarticops::auth.confirm.sent'));
        
        return view('smarticops::welcome');
    }
    
    
    /**
     * Handles a confirmation request
     *  Confirms the user if the link is valid
     * 
     * @param  Request  $request
     * @param  string  $token  The token to use for confirmation
     * 
     * @author David Vivier
     */
    protected function confirm(Request $request, $token) {
        
        $user = User::where('confirm_token', $token)->first();
        
        if ($user == null) {
            return redirect('home')->withErrors([trans('smarticops::auth.confirm.error_token')]);
        }
        
        $created = date('U', strtotime($user->confirm_created_at));
        $now = date('U');
        $diff = ($now - $created)/60; // minutes
        $max = Config::get('smarticops.confirm-expire', 60);

        if ($diff > $max) {
            $user->confirm_token = null;
            $user->save();
            return redirect('home')->withErrors([trans('smarticops::auth.confirm.error_expired')]);
        }
        
        $user->confirmed = true;
        $user->confirm_token = null;
        $user->save();
        
        Auth::login($user);
        Session::flash('info', trans('smarticops::auth.confirm.confirmed'));
        return redirect('home');
    }
}
