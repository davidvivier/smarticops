<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */

namespace Dvivier\Smarticops\Http\Controllers;

use Illuminate\Http\Request;

use Dvivier\Smarticops\Http\Requests;

use Dvivier\Smarticops\User;
use Dvivier\Smarticops\Role;
use Dvivier\Smarticops\Permission;
use App;

use Dvivier\Smarticops\Facades\Smarticops;

use Session;
use Validator;
use Auth;

/**
 *  The controller for Dvivier\Smarticops\User model.
 * 
 */
class UsersController extends Controller
{
    
    /**
     * The users index.
     * 
     * @todo pagination
     * @todo sorting
    */
    public function index() {
        
        $users = User::all();
        
        return view('smarticops::users.index', compact('users'));
    }
    
    /**
     * Prepare form to create a user
     */
    public function create() {
        
        $updating = false;
        
        $roles = Role::all();
        /*$permissions = Permission::all()
                                    ->reject(function($perm) {
                                        // "sa" permission - not visible
                                        return ($perm->id === 1);
                                      });*/
        $permissions = Permission::all();
        // removing the first permission "sa"
        //var_dump($permissions->implode('code', ','));
        //var_dump(count($permissions));
        $permissions->shift();
        //var_dump($permissions->implode('code', ','));
        //var_dump(count($permissions));
        return view('smarticops::users.edit', compact('updating', 'roles', 'permissions'));
    }
    
    /**
     * Prepare form to edit a user
     * @param  User  $user the user already existing which is about to be modified
     */
    public function edit( User $user ) {
        
        $updating = true;
        
        if (null === $user) {
            var_dump($user);
            return redirect('/users');
        }
        
        $roles = Role::all();/*
        $permissions = Permission::all()
                                    ->reject(function($perm) {
                                        // "sa" permission - not visible
                                        return ($perm->id === 1);
                                    });*/
        $permissions = Permission::all();
        // removing the first permission "sa"
        $permissions->shift();
        
        foreach($roles as $role){
            
        }
        
        return view('smarticops::users.edit', compact('updating', 'user', 'roles', 'permissions'));
    }
    
    
    /**
     * Handles a create request, then calls the post() method
     * 
     * @param  Request  $request
     * 
     * @see UsersController::post() post()
     */
    public function create_post(Request $request) {
        /*
        $this->validate($request, [
                            'first_name' => 'required',
                            'last_name' => 'required',
                            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
                                    //  we check that the email is unique among only non-deleted users
                            ]); */
        
        $rules = [
                    'first_name'    => 'required',
                    'last_name'     => 'required',
                    'email'         => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
                                    //  we check that the email is unique among only non-deleted users
                    ];
                    
        $messages = [
                        'required' => trans('smarticops::validation.required'),
                        'unique'   => trans('smarticops::validation.custom.email.unique'),
                        'email'    => trans('smarticops::validation.email'),
                        ];
                        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        if ($validator->fails()) {
            return redirect('users/create')->withErrors($validator)->withInput();
        }
        
        $user = new User;

        return $this->post($user, $request, false);
    }
    
    /**
     * Handles an edit request, then calls the post() method
     * 
     * @param  Request  $request
     * @param  User  $user
     * 
     * @see UsersController::post() post()
     */
    public function edit_post(Request $request, User $user) {
        /*
        $this->validate($request, [
                            'first_name' => 'required',
                            'last_name' => 'required',
                            'email' => 'required|email|unique:users,email,'.$user->id.',id,deleted_at,NULL'
                                    // we check that the email is unique among only non-deleted users
                                    //  AND we omit the user itself
                            ]);*/
        
        $rules = [
                    'first_name'    => 'required',
                    'last_name'     => 'required',
                    'email' => 'required|email|unique:users,email,'.$user->id.',id,deleted_at,NULL'
                                    //  we check that the email is unique among only non-deleted users
                                    //  AND we omit the user itself
                    ];
                    
        $messages = [
                        'required' => trans('smarticops::validation.required'),
                        'unique'   => trans('smarticops::validation.custom.email.unique'),
                        'email'    => trans('smarticops::validation.email'),
                        ];
                        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        if ($validator->fails()) {
            return redirect('users/edit/'.$user->id)->withErrors($validator)->withInput();
        }

        return $this->post($user, $request, true);
    }
    
    /**
     * Processes the creation/update of a user
     * 
     * @param  User $user The user, already existing or freshly created
     * @param  Request  $request
     * @param  boolean  $updating  Whether it's an update (true) or the creation (false) of the user.
     * 
     */
    private function post(User $user, $request, $updating = false) {
        
        if ($updating) {
            $old = clone $user;
        }
        
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        
        $res = $user->save();
        

        
        $roles = $request->roles;
        
        // revoke all roles, then assign the wanted roles
        $user->revokeAllRoles();
        if (count($roles) > 0) {
            foreach($roles as $role) {
                $user->assignRole($role);
            }
        }
        
        // remove all permissions
        $user->permissions()->detach();
        // give requested permissions
        $codes = $request->codes;
        if (count($codes) > 0) {
            foreach($codes as $permission) {
                $user->givePermission($permission);
            }
        }
        if ($user->id === 1) {
            $user->givePermission('sa');
        }

        if ($res) {
            if ($updating) {
                Session::flash('info', trans('smarticops::users.info.updated', ['full_name' => $user->fullName()]));
                Smarticops::logUserUpdated($user, $old, true);
            } else {
                Session::flash('info', trans('smarticops::users.info.created', ['full_name' => $user->fullName()]));
                Smarticops::logUserCreated($user, true);
            }
        }
        else {
            if ($updating) {
                Session::flash('info', trans('smarticops::users.error.creating'));
                Smarticops::logUserCreated($user, false);
            }
            else {
                Session::flash('info', trans('smarticops::users.error.updating'));
                Smarticops::logUserUpdated($user, false);
            }
        }
        //var_dump('post-end');
        //Session::put('func', 'post');
        /*
        if (Session::get('func') == 'testCreate()') {
            var_dump(Auth::user()->permissions->all());
            var_dump(Auth::user()->getPermissions()->all());
            Auth::user()->loadPermissions();
            var_dump(Auth::user()->getPermissions()->all());
        }*/
        //Auth::user()->loadPermissions();
        return redirect('/users');
    }
    
    /**
     * Handles a delete request.
     * 
     * The user will be softly deleted.
     * 
     * @param  User  $user  
     */
    public function delete(User $user) {
        
        if ($user->id === 1) {
            return redirect('/users')->withErrors([trans('smarticops::users.delete.sa')]);
        }
        
        $res = $user->delete();
        
        if ($res) {
            Session::flash('info', trans('smarticops::users.info.deleted', ['full_name' => $user->fullName()]));
        } else {
            Session::flash('info', trans('smarticops::users.error.deleting'));
        }
        Smarticops::logUserDeleted($user, $res);
        
        return redirect('/users');
    }
}
