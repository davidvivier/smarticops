<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */

namespace Dvivier\Smarticops\Http\Controllers;

use Illuminate\Http\Request;

use Dvivier\Smarticops\Http\Requests;

/**
 * Handles some pages of the application.
 * 
 */
class PagesController extends Controller
{
    
    /**
     * The welcome page.
     */
    public function welcome() {
        return view('smarticops::welcome');
    }
    
    /**
     * Access to the administration panel.
     * 
     * Permission to access this panel managed by Administrator Middleware.
     * 
     * @see Dvivier\Smarticops\Http\Middleware\Administrator Administrator Middleware
     */
    public function admin() {
        return view('smarticops::admin');
    }
    
}
