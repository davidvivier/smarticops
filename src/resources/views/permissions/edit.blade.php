
@extends('smarticops::layouts.app')

@if (isset($updating) and $updating)
    @section( 'title',  trans('smarticops::permissions.edit.title')  )
@else
    @section( 'title',  trans('smarticops::permissions.create.title')  )
@endif

@section( 'content' )


    
    @include('smarticops::errors.show')
    
    <div>
        @if (isset($updating) and $updating)
                {{ Form::model( $permission,
                                array( 
                                    'url' => 'permissions/edit/'.$permission->id,
                                    'method' => 'post',
                                    ) ) }}
        @else
            {{ 
                Form::open( array( 
                                'url' => 'permissions/create',
                                'method' => 'post',
                                ) ) }}
        @endif
        
        <div>
            {{ Form::label('code', trans('smarticops::permissions.code')) }}
            {{ Form::text('code') }}
        </div>
        
        <div>
            {{ Form::label('description', trans('smarticops::permissions.description')) }}
            {{ Form::text('description') }}
        </div>
        
        @if (isset($updating) and $updating)
            {{ Form::hidden('id', $permission->id) }}
        @endif
        
        <div>
            @if (isset($updating) and $updating)
                {{ Form::submit(trans('smarticops::permissions.edit.submit')) }}
            @else
                {{ Form::submit(trans('smarticops::permissions.create.submit')) }}
            @endif
        </div>
        
        {{ Form::close() }}
    </div>
    
    {{ link_to('/permissions', trans('smarticops::permissions.link.back')) }}
@endsection
