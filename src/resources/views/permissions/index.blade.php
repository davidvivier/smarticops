
@extends('smarticops::layouts.app')

@section( 'title',  trans('smarticops::permissions.list.title')  )

@section( 'content' )
    
    @include('smarticops::errors.show')
    
   {{ link_to('/permissions/create', trans('smarticops::permissions.link.create')) }}
    
    @if ( count($permissions) > 0 ) 
        <table>
            <thead>
                <tr>
                    <th>
                        {{ trans('smarticops::permissions.code') }}
                    </th>
                    <th>
                        {{ trans('smarticops::permissions.description') }}
                    </th>
                    <th>
                        {{ trans('smarticops::permissions.list.actions') }}
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permissions as $permission) 
                    
                    <tr>
                        <td>
                            {{ $permission->code }}
                        </td>
                        <td>
                            {{ $permission->description }}
                        </td>
                        <td>
                            {{ link_to('permissions/edit/'.$permission->id, trans('smarticops::permissions.link.edit')) }}
                            &nbsp; | &nbsp;
                            {{-- link_to('permissions/delete/'.$permission->id, trans('smarticops::permissions.link.delete')) --}}
                            @if (count($permission->users) == 0 and count($permission->roles) == 0)
                                <a href="#" onclick="ask_delete({{$permission->id}}, '{{$permission->code}}');" >
                                    {{trans('smarticops::permissions.link.delete')}}
                                </a>
                            @else
                                <span title="{{trans_choice('permissions.list.nodelete', 
                                                                count($permission->users), 
                                                                [    'n_users' => count($permission->users),
                                                                     'n_roles' => count($permission->roles),
                                                            ])
                                                }}">         
                                    {{trans('smarticops::permissions.link.delete')}}
                                </span>
                            @endif
                        </td>
                    </tr>
                
                @endforeach 
            </tbody>
        </table>
        <script>
            function ask_delete(id, code) {
                
                if (confirm('{{trans('smarticops::smarticops::permissions.confirm.delete')}}'+code+' ?')) {
                    window.location.replace('permissions/delete/'+id);
                }
            }
        </script>
    @else
        <div>
            {{ trans('smarticops::permissions.list.zero') }}
        </div>
    @endif
    
@endsection