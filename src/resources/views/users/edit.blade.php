
@extends('smarticops::layouts.app')

@if (isset($updating) and $updating)
    @section( 'title',  trans('smarticops::users.edit.title')  )
@else
    @section( 'title',  trans('smarticops::users.create.title')  )
@endif

@section( 'content' )
    
    @include('smarticops::errors.show')
    
    <div>
        @if (isset($updating) and $updating)
                {{ Form::model( $user,
                                array( 
                                    'url' => 'users/edit/'.$user->id,
                                    'method' => 'post',
                                    ) ) }}
        @else
            {{ 
                Form::open( array( 
                                'url' => 'users/create',
                                'method' => 'post',
                                ) ) }}
        @endif
        
        <div>
            {{ Form::label('first_name', trans('smarticops::users.first_name')) }}
            {{ Form::text('first_name') }}
        </div>
        
        <div>
            {{ Form::label('last_name', trans('smarticops::users.last_name')) }}
            {{ Form::text('last_name') }}
        </div>
        
        <div>
            {{ Form::label('email', trans('smarticops::users.email')) }}
            {{ Form::email('email') }}
        </div>
        
        @if (isset($updating) and $updating)
            {{ Form::hidden('id', $user->id) }}
        @endif
        
        <div>
            @if (isset($updating) and $updating)
                {{ Form::submit(trans('smarticops::users.edit.submit')) }}
            @else
                {{ Form::submit(trans('smarticops::users.create.submit')) }}
            @endif
        </div>
        
        <div>
            <h2>
                {{ trans('smarticops::users.edit.roles') }}
            </h2>
            

            

            @foreach($roles as $key => $role) 
                <span>
                    <input type="checkbox" id="role-{{$role->name}}" name="roles[{{$key}}]" value="{{$role->name}}" onclick="role_clicked('{{$role->name}}');" <?php  if (isset($updating) and $updating) {
                                                                                                                                    if ($user->is($role->name)) {     
                                                                                                                                        echo 'checked';
                                                                                                                                    } 
                                                                                                                                }
                                                                                                                                ?>
                                                                                                                                />
                    <label for="role-{{$role->name}}" title="{{$role->description}}">
                        {{ $role->name }}
                    </label>
                </span>
                
                <script>
                    function check_perm_role_{{ $role->name }}() {
                        if (document.getElementById("role-{{$role->name}}").checked == true) {
                            @foreach($role->permissions as $permission)
                                document.getElementById("perm-{{ $permission->code }}").checked = true;
                            @endforeach
                        }
                    }
                </script>
                
            @endforeach
            
            <script>
                function role_clicked(roleName) {
                    //alert(document.getElementById("role-"+roleName));
                    var checked = document.getElementById("role-"+roleName).checked ? true : false;
                    
                    // uncheck all permissions
                    
                    @foreach($permissions as $permission) 
                        document.getElementById("perm-{{ $permission->code }}").checked = false;
                    @endforeach
                    
                    @foreach ($roles as $role)
                        // for each role, we test whether it is checked or not
                        checked = document.getElementById("role-{{$role->name}}").checked ? true : false;
                        
                        if (checked) {
                            // if so, we mark the corresponding permissions
                            check_perm_role_{{$role->name}}();
                            // @see function check_perm_role_ 
                        }
                    @endforeach
                    
                    //alert(checked);
                    if (checked) {
                        eval('check_perm_role_'+roleName+'()');
                        
                    }
                    
                }
            </script>
            
        </div>
        
        <script>
            function cb(role_id) {
                //alert(role_id);
            }
        </script>
        
        <div>
            <h2>
                {{ trans('smarticops::users.edit.permissions') }}
            </h2>
            
            
            @foreach($permissions as $key => $permission) 
                <span>
                    {{-- Form::checkbox('code', $permission->description, true) --}}
                    <input id="perm-{{$permission->code}}" type="checkbox" name="codes[{{$key}}]" value="{{$permission->code}}" <?php   if (isset($updating) and $updating) {
                                                                                                                                    if ($user->hasPermission($permission->code, true)) {    
                                                                                                                                                    // $fromDB = true becaus the user we are looking at is 
                                                                                                                                                    //  not necessarily the logged in user, @see hasPermission
                                                                                                                                        echo 'checked';
                                                                                                                                    } 
                                                                                                                                }
                                                                                                                                ?>
                                                                                                                                />
                        
                    <label for="perm-{{$permission->code}}" title="{{$permission->description}}">
                        {{ $permission->code }}
                    </label>
                </span>
            @endforeach
        </div>
        
        <div>
            @if (isset($updating) and $updating)
                {{ Form::submit(trans('smarticops::users.edit.submit')) }}
            @else
                {{ Form::submit(trans('smarticops::users.create.submit')) }}
            @endif
        </div>
        
        
        {{ Form::close() }}
    </div>
    
    {{ link_to('/users', trans('smarticops::users.link.back')) }}
@endsection