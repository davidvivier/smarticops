
@extends('smarticops::layouts.app')

@section( 'title',  trans('smarticops::users.list.title')  )

@section( 'content' )

    @include('smarticops::errors.show')

   {{ link_to('/users/create', trans('smarticops::users.link.create')) }}

    @if ( count($users) > 0 ) 
        <table>
            <thead>
                <tr>
                    <th>
                        {{ trans('smarticops::users.full_name') }}
                    </th>
                    <th>
                        {{ trans('smarticops::users.list.roles') }}
                    </th>
                    <th>
                        {{ trans('smarticops::users.email') }}
                    </th>
                    <th>
                        {{ trans('smarticops::users.list.actions') }}
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user) 
                    
                    <tr>
                        <td>
                            {{ $user->fullName() }}
                        </td>
                        <td>
                            <?php
                                $first_el = true;
                                foreach($user->roles as $role) {
                                    if ( ! $first_el) {
                                        echo '&nbsp;|&nbsp;';
                                    }
                                    $first_el = false;
                                    echo '<span>'.$role->name.'</span>';
                                }
                                ?>
                        </td>
                        <td>
                            {{ $user->email }}
                        </td>
                        <td>
                            {{ link_to('users/edit/'.$user->id, trans('smarticops::users.link.edit')) }}
                            {{-- Form::open(array(
                                                'action' => array( 'UsersController@edit', $user->id ),
                                                'method' => 'get',
                                                )) --}}
                            {{-- Form::submit(trans('smarticops::general.edit')) --}}
                            
                            @if ($user->id !== 1)  
                                {{-- the superadmin user cannot be deleted  --}}
                                &nbsp; | &nbsp;
                                
                                {{-- link_to('users/delete/'.$user->id, trans('smarticops::users.link.delete'), [ 'onclick' => 'askDelete();'] ) --}}
                                <a href="#" onclick="ask_delete({{$user->id}}, '{{$user->fullName()}}');" >
                                    {{trans('smarticops::users.link.delete')}}
                                </a>
                            @endif
                            
                        </td>
                    </tr>
                
                @endforeach 
            </tbody>
        </table>
        <script>
            function ask_delete(id, name) {
                if (confirm('{{trans('smarticops::users.confirm.delete')}}'+name+' ?')) {
                    window.location.replace('users/delete/'+id);
                }
            }
        </script>
    @else
        <div>
            {{ trans('smarticops::users.list.zero') }}
        </div>
    @endif
    
@endsection