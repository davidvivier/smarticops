

@extends('smarticops::layouts.app')

@section( 'title',  trans('smarticops::general.admin.title') )

@section( 'content' )


<div class="container">
    @if (Session::has('info'))
        <div>
            {{ Session::get('info') }}
        </div>
    @endif
    
                    <div>
                        <ul>
                            <li>
                                {{ link_to('permissions', trans('smarticops::general.admin.permissions')) }}
                            </li>
                            <li>
                                {{ link_to('roles', trans('smarticops::general.admin.roles')) }}
                            </li>
                            <li>
                                {{ link_to('users', trans('smarticops::general.admin.users')) }}
                            </li>
                        </ul>
                    </div>


@endsection
