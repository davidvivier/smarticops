@if (App::isLocale('fr'))
    Bonjour {{$user->first_name}},
    <br />
    <br />Cliquez sur ce lien pour r&eacute;initialiser votre mot de passe : <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>
    <br />Si le lien ne fonctionne pas, copiez et collez-le dans votre navigateur.
    <br />
@else 
    Hi {{$user->first_name}},
    <br />
    <br />Click here to reset your password : <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>
    <br />If the link doesn't work, copy and paste it in your browser.
    <br />
    <br />
@endif