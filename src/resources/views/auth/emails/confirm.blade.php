@if (App::isLocale('fr'))
    Bonjour {{$user->first_name}},
    <br />
    <br />Veuillez confirmer votre compte chez Laravel en cliquant sur ce lien : <a href="{{ $url }}">{{ $url }}</a>
    <br />Si le lien ne fonctionne pas, copiez et collez-le dans votre navigateur.
    <br />
@else
    Hi {{ $user->first_name }},
    <br />
    <br />Please confirm your account by clicking this link : <a href="{{ $url }}">{{ $url }}</a>
    <br />If the link doesn't work, copy and paste it in your browser.
@endif