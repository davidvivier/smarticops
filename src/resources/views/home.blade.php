@extends('smarticops::layouts.app')

@section('title', trans('smarticops::general.dashboard'))

@section('content')


@unless ($user->confirmed)
    <div>
        {{trans('smarticops::auth.confirm.not')}}
        <a href="{{ url('sendtoken/'.$user->id) }}">
            {{trans('smarticops::auth.confirm.send')}}
        </a>
    </div>
@endunless

{{trans('smarticops::general.logged_in')}}

@endsection
