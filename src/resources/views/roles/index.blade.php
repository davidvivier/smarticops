
@extends('smarticops::layouts.app')

@section( 'title',  trans('smarticops::roles.list.title')  )

@section( 'content' )

    @include('smarticops::errors.show')
    
   {{ link_to('/roles/create', trans('smarticops::roles.link.create')) }}
    
    @if ( count($roles) > 0 ) 
        <table>
            <thead>
                <tr>
                    <th>
                        {{ trans('smarticops::roles.name') }}
                    </th>
                    <th>
                        {{ trans('smarticops::roles.description') }}
                    </th>
                    <th>
                        {{ trans('smarticops::roles.list.actions') }}
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $role) 
                    
                    <tr>
                        <td>
                            {{ $role->name }}
                        </td>
                        <td>
                            {{ $role->description }}
                        </td>
                        <td>
                            {{ link_to('roles/edit/'.$role->id, trans('smarticops::roles.link.edit')) }}
                                &nbsp; | &nbsp;
                            @if (count($role->users) == 0)
                                {{-- link_to('roles/delete/'.$role->id, trans('smarticops::roles.link.delete')) --}}
                                <a href="#" onclick="ask_delete({{$role->id}}, '{{$role->name}}');" >
                                    {{trans('smarticops::roles.link.delete')}}
                                </a>
                            @else
                                <span title="{{trans_choice('smarticops::roles.list.nodelete', count($role->users), ['n' => count($role->users)])}}">         
                                    {{trans('smarticops::roles.link.delete')}}
                                </span>
                            @endif
                            
                        </td>
                    </tr>
                
                @endforeach 
            </tbody>
        </table>
        <script>
            function ask_delete(id, name) {
                
                if (confirm('{{trans('smarticops::roles.confirm.delete')}}'+name+' ?')) {
                    window.location.replace('roles/delete/'+id);
                }
            }
        </script>
    @else 
        <div>
            {{ trans('smarticops::roles.list.zero') }}
        </div>
    @endif
@endsection