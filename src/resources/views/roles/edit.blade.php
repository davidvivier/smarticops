
@extends('smarticops::layouts.app')

@if (isset($updating) and $updating)
    @section( 'title',  trans('smarticops::roles.edit.title')  )
@else
    @section( 'title',  trans('smarticops::roles.create.title')  )
@endif

@section( 'content' )


    
    @include('smarticops::errors.show')
    
    <div>
        @if (isset($updating) and $updating)
                {{ Form::model( $role,
                                array( 
                                    'url' => 'roles/edit/'.$role->id,
                                    'method' => 'post',
                                    ) ) }}
        @else
            {{ 
                Form::open( array( 
                                'url' => 'roles/create',
                                'method' => 'post',
                                ) ) }}
        @endif
        
        <div>
            {{ Form::label('name', trans('smarticops::roles.name')) }}
            {{ Form::text('name') }}
        </div>
        
        <div>
            {{ Form::label('description', trans('smarticops::roles.description')) }}
            {{ Form::text('description') }}
        </div>
        
        
        <div>
            <h2>
                {{ trans('smarticops::roles.edit.permissions') }}
            </h2>
            <div>
                {{ trans('smarticops::roles.edit.permissions.note') }}
            </div>
            
            @foreach($permissions as $key => $permission) 
                <span>
                    <input id="perm-{{$permission->code}}" type="checkbox" name="codes[{{$key}}]" value="{{$permission->code}}" <?php   if (isset($updating) and $updating) {
                                                                                                                                    if ($role->hasPermission($permission->code)) {     
                                                                                                                                        echo 'checked';
                                                                                                                                    } 
                                                                                                                                }
                                                                                                                                ?>
                                                                                                                                />
                    <label for="perm-{{$permission->code}}" title="{{$permission->description}}">
                        {{ $permission->code }}
                    </label>
                </span>
            @endforeach
        </div>
        
        @if (isset($updating) and $updating)
            {{ Form::hidden('id', $role->id) }}
        @endif
        
        <div>
            @if (isset($updating) and $updating)
                {{ Form::submit(trans('smarticops::roles.edit.submit')) }}
            @else
                {{ Form::submit(trans('smarticops::roles.create.submit'), ['name' => 'submit', 'id' => 'submit'] ) }}

            @endif
        </div>
        
        {{ Form::close() }}
    </div>
    
    {{ link_to('/roles', trans('smarticops::roles.link.back')) }}
@endsection