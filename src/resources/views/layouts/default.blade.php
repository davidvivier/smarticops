<!DOCTYPE html>
<html>
    <head>
        
        <title>@yield( 'title' ) - Comiffo </title>
        
        <link rel="stylesheet" href="{{ elixir('css/app.css') }}" type="text/css" />

    </head>
    <body>
        
        <div class="container">
            <div>
                {{ link_to('/lang/en', 'en') }}
                &nbsp;/&nbsp;
                {{ link_to('/lang/fr', 'fr') }}
                
            </div>
            <div>
                @if (Auth::check())
                    Connected as {{Auth::user()->fullName()}} ({{Auth::user()->id}})
                @else
                    Not connected
                @endif
            </div>
            {{ link_to('admin', trans('smarticops::general.admin.link') ) }}
            @include('errors.show')
            @if (Session::has('info'))
                <div>
                    {{ Session::get('info') }}
                </div>
            @endif

            @yield( 'content' )
        </div>
        
    </body>
</html>

