<?php
// fr
return [
    
    'code'              => 'Code',
    'description'       => 'Description',
    
    
    
    'list.title'        => 'Liste des Permissions',
    'list.actions'      => 'Actions',
    'list.zero'         => 'Aucune permission trouv&eacute;e.',
    'list.nodelete'     => 'Cette permission est attribu&eacute;e &agrave; :n_users utilisateurs(s) et :n_roles r&ocirc;le(s) : elle ne peut pas &ecirc;tre supprim&eacute;e.',
     
    'create.title'      => 'Cr&eacute;ation d\'une Permission',
    'create.submit'     => 'Create',
    
    'edit.title'        => 'Modification d\'une Permission',
    'edit.submit'       => 'Enregistrer les modifications',
    
    'link.create'       => 'Cr&eacute;er une nouvelle Permission',
    'link.back'         => 'Retour à la liste des permissions',
    'link.edit'         => 'Modifier',
    'link.delete'       => 'Supprimer',
    
    'info.created'  => 'La permission :code a &eacute;t&eacute; cr&eacute;&eacute;e.',
    'info.updated'  => 'La permission :code a &eacute;t&eacute; mise &agrave; jour.',
    'info.deleted'  => 'La permission :code a &eacute;t&eacute; supprim&eacute;e.',
    
    'error.creating'   => 'Une erreur s\'est produite lors de la cr&eacute;ation, veuillez r&eacute;essayer.',
    'error.updating'   => 'Une erreur s\'est produite lors de la mise &agrave; jour, veuillez r&eacute;essayer.',
    'error.deleting'   => 'Une erreur s\'est produite lors de la suppression, veuillez r&eacute;essayer.',
    
    //'confirm.delete'    => '&Ecirc;tes-vous s&ucirc;r de vouloir supprimer la permission \: ',
    'confirm.delete'    => 'Etes-vous sur de vouloir supprimer la permission \: ',
    
    
    'error.not_allowed' => 'Vous n\'&ecirc;tes pas autoris&eacute; &agrave; effectuer cette action.',
    
    ];