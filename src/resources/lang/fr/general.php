<?php
// fr
return [    
    
    'home'      => 'Accueil',
    'welcome'   => 'Bienvenue',
    'dashboard' => 'Tableau de bord',
    'logged_in' => 'Vous &ecirc;tes connect&eacute; !',
    
    'landing'   => 'Page d\'arriv&eacute;e de votre application.',
    
    'edit'      => 'Modifier',
    'delete'    => 'Supprimer',
    
    'errors'    => 'Il y a une erreur dans le formulaire : | Il y a des erreurs dans le formulaire :',
    
    'csrf_mismatch' => 'D&eacute;sol&eacute;, il y a une erreur de validation de votre requ&ecirc;te, veuillez recommencer.',
    
    'admin.link'    => 'Retour &agrave; l\'interface d\'administration',
    
    'admin.title'   => 'Administration',
    'admin.users'   => 'Utilisateurs',
    'admin.permissions' => 'Permissions',
    'admin.roles'       => 'R&ocirc;les',
    
    ];