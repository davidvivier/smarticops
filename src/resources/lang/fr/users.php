<?php
// fr
return [
    
    'first_name'        => 'Pr&eacute;nom',
    'last_name'         => 'Nom',
    'full_name'         => 'Nom complet',
    'email'             => 'Courriel',
    
    'list.title'        => 'Liste des Utilisateurs',
    'list.roles'        => 'R&ocirc;les',
    'list.actions'      => 'Actions',
    'list.zero'         => 'No users where found.',
     
    'create.title'         => 'Cr&eacute;er Utilisateur',
    'create.submit'     => 'Cr&eacute;er',
    
    'edit.title'        => 'Modifier un Utilisateur',
    'edit.submit'       => 'Enregistrer les modifications',
    'edit.roles'        => 'Assigner un ou plusieurs r&ocirc;les &agrave; l\'utilisateur',
    'edit.permissions'  => 'Donner des permissions &agrave; l\'utilisateur',
    
    'link.create'       => 'Cr&eacute;er un nouvel utilisateur',
    'link.back'         => 'Retour &agrave; la liste des utilisateurs',
    'link.edit'         => 'Modifier',
    'link.delete'       => 'Supprimer',
    
    'info.created'      => 'L\'utilisateur :full_name a &eacute;t&eacute; cr&eacute;&eacute;',
    'info.updated'      => 'L\'utilisateur :full_name a &eacute;t&eacute; mis &agrave; jour.',
    'info.deleted'      => 'L\'utilisateur :full_name a &eacute;t&eacute; supprim&eacute;.',
    
    'error.creating'   => 'Une erreur s\'est produite lors de la cr&eacute;ation, veuillez r&eacute;essayer.',
    'error.updating'   => 'Une erreur s\'est produite lors de la mise &agrave; jour, veuillez r&eacute;essayer.',
    'error.deleting'   => 'Une erreur s\'est produite lors de la suppression, veuillez r&eacute;essayer.',
    
    'confirm.delete'    => "Etes-vous sur de vouloir supprimer l utilisateur \: ",
    
    'delete.sa'       => 'Cet utilisateur est un enregistrement syst&egrave;me, il ne peut pas &ecirc;tre supprim&eacute;.',
    ];
    
    