<?php
// fr
return [
    
    'name'          => 'Nom',
    'description'          => 'Description',
    
    
    
    'list.title'    => 'Liste des R&ocirc;les',
    'list.actions'  => 'Actions',
    'list.zero'     => 'Aucun r&ocirc;le trouv&eacute;.',
    'list.nodelete' => '1 utilisateur est associ&eacute; &agrave; ce r&ocirc;le, il ne peut pas &ecirc;tre supprim&eacute;. | :n utilisateurs sont associ&eacute;s &agrave; ce r&ocirc;le, il ne peut pas &ecirc;tre supprim&eacute;.',
    
    'create.title'  => 'Cr&eacute;er un R&ocirc;le',
    'create.submit' => 'Creer',
    
    'edit.title'    => 'Modifier un R&ocirc;le',
    'edit.submit'   => 'Enregistrer les modifications',
    'edit.permissions'  => 'Attribuer des Permissions',
    'edit.permissions.note'   =>  'Les permissions s&eacute;lectionn&eacute;es seront attribu&eacute;es aux utilisateurs auquels ce r&ocirc;le sera assign&eacute;. Les utilisateurs existants ne seront pas affect&eacute;s.',
    
    'link.create'   => 'Cr&eacute;er un nouveau R&ocirc;le',
    'link.back'     => 'Retour &agrave; la liste des R&ocirc;les',
    'link.edit'     => 'Modifier',
    'link.delete'   => 'Supprimer',
    
    'info.created'  => 'Le r&ocirc;le :name a &eacute;t&eacute; cr&eacute;&eacute;.',
    'info.updated'  => 'Le r&ocirc;le :name a &eacute;t&eacute; mis &agrave; jour.',
    'info.deleted'  => 'Le r&ocirc;le :name a &eacute;t&eacute; supprim&eacute;.',
    
    'error.creating'   => 'Une erreur s\'est produite lors de la cr&eacute;ation, veuillez r&eacute;essayer.',
    'error.updating'   => 'Une erreur s\'est produite lors de la mise &agrave; jour, veuillez r&eacute;essayer.',
    'error.deleting'   => 'Une erreur s\'est produite lors de la suppression, veuillez r&eacute;essayer.',
    
    'confirm.delete'    => 'Etes-vous sur de vouloir supprimer le role \: ',

    ];