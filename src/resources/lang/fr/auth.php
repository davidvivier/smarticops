<?php
// fr
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ces identifiants ne correspondent &agrave; aucun de nos enregistrements.',
    'throttle' => 'Vous avez atteint le nombre maximum de tentatives ; veuillez attendre :seconds secondes avant de r&eacute;essayer. ',
    
    'title.login'       => 'Connexion',
    'title.register'    => 'Inscription',
    
    'link.login'        => 'Connexion',
    'link.register'     => 'Inscription',
    'link.logout'       => 'D&eacute;connexion',
    

    'form.email'        => 'Courriel',
    'form.password'     => 'Mot de passe',
    'form.minimum'      =>  Config::get('smarticops.password.min', 8).' caract&egrave;res minimum',
    'form.confirm_password' => 'Confirmez le mot de passe',
    
    'form.login'        => 'Connexion',
    'form.register'     => 'Inscription',
    
    'form.remember'     => 'Se souvenir de moi',
    
    'form.forgot'       => 'Mot de passe oubli&eacute; ?',
    
    'confirm.not'       => 'Votre compte n\'est pas confirm&eacute;.',
    'confirm.send'      => 'Envoyer le lien de confirmation',
    'confirm.subject'   => 'Confirmez votre compte',
    'confirm.sent'      => 'Lien envoy&eacute;, v&eacute;rifiez votre bo&icirc;te de r&eacute;ception et votre courrier ind&eacute;sirable.',
    'confirm.have'      => 'Vous devez confirmer votre compte',
    'confirm.confirmed' => 'Votre compte a &eacute;t&eacute; correctement confirm&eacute;.',
    'confirm.error_expired'   => 'Erreur : ce lien a expir&eacute;.',
    'confirm.error_token' => 'Error : ce lien est invalide.',
    
];
