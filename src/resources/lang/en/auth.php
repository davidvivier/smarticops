<?php
// en
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    
    'title.login'       => 'Login',
    'title.register'    => 'Register',
    
    'link.login'        => 'Login',
    'link.register'     => 'Register',
    'link.logout'       => 'Logout',
    

    'form.email'        => 'Email',
    'form.password'     => 'Password',
    'form.minimum'      => 'minimum '.Config::get('smarticops.password.min', 8).' characters',
    'form.confirm_password' => 'Confirm password',
    
    'form.login'        => 'Login',
    'form.register'     => 'Register',
    
    'form.remember'     => 'Remember Me',
    
    'form.forgot'       => 'I forgot my password',
    
    'confirm.not'       => 'Your account is not confirmed.',
    'confirm.send'      => 'Send confirmation link',
    'confirm.subject'   => 'Confirm your account',
    'confirm.sent'      => 'Email sent, check your inbox and spam folder',
    'confirm.have'      => 'You have to confirm your account.',
    'confirm.confirmed' => 'Your account was successfully confirmed',
    'confirm.error_expired'   => 'Error : this link has expired.',
    'confirm.error_token' => 'Error : invalid token',
];
