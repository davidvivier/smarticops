<?php
// en
return [    
    
    'home'      => 'Home',
    'welcome'   => 'Welcome',
    'dashboard' => 'Dashboard',
    'logged_in' => 'You are logged in!',
    
    'landing'   => 'Your Application\'s Landing Page.',
    
    'edit'      => 'Edit',
    'delete'    => 'Delete',
    
    'errors'    => 'There is an error |There are errors ',
    
    'csrf_mismatch' => 'Sorry, there was an error validating your request. Please retry.',
    
    'admin.link'    => 'Back to administration interface',
    
    'admin.title'   => 'Administration',
    'admin.users'   => 'Users',
    'admin.permissions' => 'Permissions',
    'admin.roles'       => 'Roles',
    
    ];