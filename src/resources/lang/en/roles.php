<?php
// en
return [
    
    'name'          => 'Name',
    'description'          => 'Description',
    
    
    
    'list.title'    => 'List of Roles',
    'list.actions'  => 'Actions',
    'list.zero'     => 'No roles found.',
    'list.nodelete' => '1 user is associated to this role, it cannot be deleted. | :n users are associated to this role, it cannot be deleted.',
    
    'create.title'  => 'Create Role',
    'create.submit' => 'Create',
    
    'edit.title'    => 'Edit Role',
    'edit.submit'   => 'Save Changes',
    'edit.permissions' => 'Give Permissions',
    'edit.permissions.note' => 'The selected permissions will be automatically given to the users being given this role. The existing users will not be affected.',      
    
    'link.create'   => 'Create new Role',
    'link.back'     => 'Back to Roles list',
    'link.edit'     => 'Edit',
    'link.delete'   => 'Delete',
    
    'info.created'  => 'The role :name has been created.',
    'info.updated'  => 'The role :name has been updated.',
    'info.deleted'  => 'The role :name has been deleted.',
    
    'error.creating'    => 'There was an error while creating the role, please retry.',
    'error.creating'    => 'There was an error while updating the role, please retry.',
    'error.deleting'   => 'There was an error while deleting the role, please retry.',
    
    'confirm.delete'    => 'Are you sure you want to delete the role \: ',
    
    ];