<?php
// en
return [
    
    'code'              => 'Code',
    'description'       => 'Description',
    
    
    
    'list.title'        => 'List of Permissions',
    'list.actions'      => 'Actions',
    'list.zero'         => 'No permissions found.',
    'list.nodelete'     => 'This permission is associated with :n_users user(s) and :n_roles role(s) : it cannot be deleted now.',
     
    'create.title'      => 'Create a Permission',
    'create.submit'     => 'Create',
    
    'edit.title'        => 'Edit a Permission',
    'edit.submit'       => 'Save Changes',
    
    'link.create'       => 'Create new Permission',
    'link.back'         => 'Back to permissions list',
    'link.edit'         => 'Edit',
    'link.delete'       => 'Delete',
    
    'info.created'      => 'The permission :code has been created.',
    'info.updated'      => 'The permission :code has been updated.',
    'info.deleted'      => 'The permission :code has been deleted.',
    
    'error.creating'    => 'There was an error while creating the permission, please retry.',
    'error.creating'    => 'There was an error while updating the permission, please retry.',
    'error.deleting'    => 'There was an error while deleting the permission, please retry.',
    
    'confirm.delete'    => 'Are you sure you want to delete this permission : ',
    
    
     'error.not_allowed' => 'You are not allowed to do this action.',
     
    ];