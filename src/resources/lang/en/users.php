<?php
// en
return [
    
    'first_name'        => 'First Name',
    'last_name'         => 'Last Name',
    'full_name'         => 'Full Name',
    'email'             => 'Email',
    
    'list.title'        => 'List of Users',
    'list.roles'        => 'Roles',
    'list.actions'      => 'Actions',
    'list.zero'         => 'No users found.',
     
    'create.title'      => 'Create a User',
    'create.submit'       => 'Create',
    
    'edit.title'        => 'Edit a User',
    'edit.submit'       => 'Save',
    'edit.roles'        => 'Assign Roles',
    'edit.permissions'  => 'Give Permissions',
    
    'link.create'       => 'Create new User',
    'link.back'         => 'Back to users list',
    'link.edit'         => 'Edit',
    'link.delete'       => 'Delete',
    
    'info.created'      => 'The user :full_name has been created.',
    'info.updated'      => 'The user :full_name has been updated.',
    'info.deleted'      => 'The user :full_name has been deleted.',
    
    'error.creating'    => 'There was an error while creating the user, please retry.',
    'error.updating'    => 'There was an error while updating the user, please retry.',
    'error.deleting'    => 'There was an error while deleting the user, please retry.',
    
    'confirm.delete'    => 'Are you sure you want to delete this user : ',
    
    'delete.sa'       => 'This record cannot be deleted.',
    ];