<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */

namespace Dvivier\Smarticops\Facades;

use Illuminate\Support\Facades\Facade;

use Dvivier\Smarticops\SmarticopsClass;

/**
 *  A facade to use the Smarticops features.
 * 
 * @see Dvivier\Smarticops\SmarticopsClass SmarticopsClass
 * 
 */
class Smarticops extends Facade {
    
    protected static function getFacadeAccessor() {
        return 'Dvivier\Smarticops\SmarticopsClass';
    }
    
}