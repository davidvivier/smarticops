<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Dvivier\Smarticops\User;
use Dvivier\Smarticops\Role;
use Dvivier\Smarticops\Permission;



class RoleTest extends TestCase
{   
    use DatabaseMigrations;
    
    public function setup() {
        parent::setup();
        $this->seed();
        Auth::login(User::find(1));
        
        $this->role = factory(Role::class)->create([

                                                                ]);
        
        $this->permission = new Permission();
        $this->permission->code = 'world.destroy';
        $this->permission->save();
        
        $this->permission = new Permission();
        $this->permission->code = 'world.dominate';
        $this->permission->save();
        
    }
    
    public function testByName() {
        
        $role = $this->role;
        $name = $role->name;
        
        $roleTest = Role::byName($name);
        
        $this->assertEquals( $role->id, $roleTest->id );
        $this->assertEquals( $role->name, $roleTest->name);
        
        $roleError = Role::byName('RoleNameWhichNeverExisted');
        $this->assertNull($roleError);
    }
    
    public function testGivePermission() {
        
        $permission = $this->permission;
        
        $role = $this->role;
        
        $res = $role->givePermission('world.dominate');
        
        $this->assertTrue($res);
        
        $this->seeInDatabase( 'roles_permissions', [
                                        'role_id' => $role->id,
                                        'permission_id' => $permission->id,
                                        'value' => true,
                                        'created_by'    => 1,
                                        ] );
        
    }
    
    public function testHasPermission() {
        $permission = $this->permission;
        
        $role = $this->role;
        
        $this->assertFalse($role->hasPermission($permission->code));
        
        $res = $role->givePermission('world.dominate');
        
        $this->assertTrue($res);

        $this->assertTrue($role->hasPermission('world.dominate'));
        $this->assertFalse($role->hasPermission('world.destroy'));
    }
    
    public function testRemovePermission() {
        $role = $this->role;
        
        $role->givePermission('world.destroy');
        $role->givePermission('world.dominate');
        
        $this->assertTrue($role->hasPermission('world.destroy'));
        $this->assertTrue($role->hasPermission('world.dominate'));
        
        $res = $role->removePermission('world.destroy');
        
        $this->assertTrue($res);
        
        $this->assertFalse($role->hasPermission('world.destroy'));
        $this->assertTrue($role->hasPermission('world.dominate'));
        
        $res = $role->removePermission('world.dominate');
        
        $this->assertFalse($role->hasPermission('world.destroy'));
        $this->assertFalse($role->hasPermission('world.dominate'));

    }
    
    public function testRemoveAllPermissions() {
        $role = $this->role;
        
        $role->givePermission('world.destroy');
        $role->givePermission('world.dominate');
        
        $this->assertTrue($role->hasPermission('world.destroy'));
        $this->assertTrue($role->hasPermission('world.dominate'));
        
        $res = $role->removeAllPermissions();
    
        $this->assertTrue($res);
        
        $this->assertFalse($role->hasPermission('world.destroy'));
        $this->assertFalse($role->hasPermission('world.dominate'));
        
    }
        
    public function testSoftDeleteRole() {
        
        $role = $this->role;
        
        $role->save();
        
        // assert that the record exists and not deleted
        $this->seeInDatabase( 'roles', [
                                                'id'         => $role->id,
                                                'deleted_at' => null 
                                                ]);

        $role->delete();
        
        // assert that the record exists AND it is (soft) deleted
        $this->seeInDatabase(       'roles', [
                                                'id'         => $role->id,
                                                ])
                ->notSeeInDatabase( 'roles', [
                                                'id'         => $role->id,
                                                'deleted_at' => null 
                                                ]);
        
    }
    
}

