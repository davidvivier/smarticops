<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */
 
namespace Dvivier\Smarticops\Http\Controllers\Auth;

use Dvivier\Smarticops\User as SmarticopsUser;
use Validator;
use Dvivier\Smarticops\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Auth;

use Config;
use Session;
use Mail;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        
        // overrides Laravel's view, use smarticops views instead.
        $this->loginView = 'smarticops::auth.login';
        $this->registerView = 'smarticops::auth.register';
    }
    
    
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:'.Config::get('smarticops.password.min',8).'|confirmed',
                ];
        $messages = [
                        'required'      => trans('smarticops::validation.required'),
                        'min'           => trans('smarticops::validation.custom.password.min'),
                        'confirmed'     => trans('smarticops::validation.custom.password.confirmed'),
                        'unique'        => trans('smarticops::validation.custom.email.unique'),
                        'max'           => trans('smarticops::validation.max.string'),
                        ];
        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = SmarticopsUser::create([
                            'first_name' => $data['first_name'],
                            'last_name' => $data['last_name'],
                            'email' => $data['email'],
                            ]);
        //   not mass-assignable fields
        $user->password = bcrypt($data['password']);

        $user->save();
        
        //$this->sendConfirmToken($user);
        
        // now user needs to confirm account
        Session::flash('info', trans('smarticops::auth.confirm.have'));
        
        // here the user is about to be logged in after registering,
        //  so we also load his permissions
        //  (the authenticated() method below is not called after registering)
        $user->loadPermissions();

        return $user;
    }
    
    /**
     *  When a user is authenticated.
     * 
     *  Method automatically called at vendor/laravel/framework/src/Illuminate/Foundation/Auth/AuthenticatesUsers.php:115
     * 
     * @param  Request  $request
     * @param  Dvivier\Smarticops\User  $user
     */ 
    protected function authenticated($request, $user) {
        //var_dump('authenticated');
        //var_dump(Auth::user());
        
        // load permissions at login
        //var_dump($user);
        
        //$smarticopsUser = SmarticopsUser::find($user->id);
        //var_dump($smarticopsUser);
        //$permissions = $smarticopsUser->loadPermissions();
        
        $permissions = Auth::user()->loadPermissions();
        //$permissions = $user->loadPermissions();
        //var_dump($permissions);
        //var_dump(Session::get('permissions'));
        //var_dump(Auth::user()->hasPermission('sa'));exit;
        // the same as in AuthenticatesUsers.php:118
        return redirect()->intended($this->redirectPath());
    }
    
    
    

    
}

