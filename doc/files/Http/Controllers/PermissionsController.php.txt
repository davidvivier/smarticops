<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */

namespace Dvivier\Smarticops\Http\Controllers;

use Illuminate\Http\Request;

use Dvivier\Smarticops\Http\Requests;

use Dvivier\Smarticops\Permission;

use Dvivier\Smarticops\Facades\Smarticops;

use Session;
use Validator;

/**
 * The controller of Dvivier\Smarticops\Permission Model.
 * 
 * @author David Vivier
 */
class PermissionsController extends Controller
{
    /**
     * The permissions index.
     * 
     * @todo pagination
     * @todo sorting
    */
    public function index() {

        $permissions = Permission::all()
                                        ->reject(function($perm) {
                                            // "sa" permission - not visible
                                            return ($perm->id === 1);
                                        });
        
        return view('smarticops::permissions.index', compact('permissions'));
    }
    
    /**
     * Prepare form to create a permission
     */
    public function create() {
        $updating = false;
        
        return view('smarticops::permissions.edit', compact('updating'));
    }
    
    /**
     * Prepare form to edit a permission
     * 
     * @param  Permission  $permission
     */
    public function edit( Permission $permission ) {
        
        $updating = true;
        
        if (null === $permission) {
            return redirect('/permissions');
        }
        
        return view('smarticops::permissions.edit', compact('updating', 'permission'));
    }
    
    /**
     * Handles a create request
     * 
     * @param  Request  $request
     */
    public function create_post(Request $request) {
        /*
        $this->validate($request, [
                                    'code' => [
                                                'required',
                                                'regex:/^[a-zA-Z0-9_\-\.]*$/',
                                                'unique:permissions,code,NULL,id,deleted_at,NULL',
                                                ],
                                    //'description' => 'required',
                                    ]);*/
        $rules = [
                    'code' => [
                                'required',
                                'regex:/^[a-zA-Z0-9_\-\.]*$/',
                                'unique:permissions,code,NULL,id,deleted_at,NULL',
                                ],
                    ];
                    
        $messages = [
                        //'required'      => 'The ":attribute" field is required.',
                        'required'      => trans('smarticops::validation.required'),
                        
                        'unique'   => trans('smarticops::validation.custom.code.unique'),
                        'regex'    => trans('smarticops::validation.custom.code.regex'),
                        ];
                        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        if ($validator->fails()) {
            return redirect('permissions/create')->withErrors($validator)->withInput();
        }
        
        $res = $permission = Permission::create([
                                                'code' => $request->code,
                                                'description' => $request->description,
                                                ]);
                                                
        Smarticops::logPermissionCreated($permission, $res, [
                                                                'code'  => $request->code,
                                                                'description'   => $request->description,
                                                                ]);
                                                                
        if ($res) {        
            Session::flash('info', trans('smarticops::permissions.info.created', ['code' => $permission->code]));
        } else {
            Session::flash('info', trans('smarticops::permissions.error.creating'));
        }

        return redirect('/permissions');
    }
    
    /**
     * Handles an edit request
     * 
     * @param  Request  $request
     * @param  Permission  $permission  The permission to edit
     */
    public function edit_post(Request $request, Permission $permission) {
        /*
        $this->validate($request, [         
                                    'code' => [
                                                'required',
                                                'regex:/^[a-zA-Z0-9_\-\.]*$/',
                                                //'alpha_dash',
                                                'unique:permissions,code,'.$permission->id.',id,deleted_at,NULL',
                                            //          Unique on column `code` IGNORING the record itself
                                            //          https://laravel.com/docs/5.2/validation#rule-unique
                                                ],
                                    //'description' => 'required',
                                    ]);*/
        
        $rules = [
                    'code' => [
                                'required',
                                'regex:/^[a-zA-Z0-9_\-\.]*$/',
                                'unique:permissions,code,'.$permission->id.',id,deleted_at,NULL',
                                ],
                    ];
                    
        $messages = [
                        //'required'      => 'The ":attribute" field is required.',
                        'required'      => trans('smarticops::validation.required'),
                        
                        'unique'   => trans('smarticops::validation.custom.code.unique'),
                        'regex'    => trans('smarticops::validation.custom.code.regex'),
                        ];
                        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        if ($validator->fails()) {
            return redirect('permissions/edit/'.$permission->id)->withErrors($validator)->withInput();
        }
        
        $old = clone $permission;
        
        $res = $permission->update([
                                'code' => $request->code,
                                'description' => $request->description,
                                ]);
                                
        Smarticops::logPermissionUpdated($permission, $old, $res);
        if ($res) {        
            Session::flash('info', trans('smarticops::permissions.info.updated', ['code' => $permission->code]));
        } else {
            Session::flash('info', trans('smarticops::permissions.error.updating'));
        }
        
        return redirect('/permissions');
    }
    
    /**
     * Handles a delete request
     * 
     * @param  Permission  $permission
     */
    public function delete(Permission $permission) {
        
        if (null === $permission) {
            Session::flash('info', trans('smarticops::permissions.error.deleting'));
            return redirect('/permissions');
        }
        
        if ($permission->id === 1) {
            // "sa" permission - not deletable
            Session::flash('info', trans('smarticops::permissions.error.deleting'));
            return redirect('/permissions');
        }
        
        if (count($permission->roles) or count($permission->users)) {
            Session::flash('info', trans_choice('smarticops::permissions.list.nodelete', 
                                                                count($permission->users), 
                                                                [    'n_users' => count($permission->users),
                                                                     'n_roles' => count($permission->roles),
                                                            ]));
            return redirect('/permissions');
        }
        
        $res = $permission->delete();
        
        Smarticops::logPermissionDeleted($permission, $res);
        if ($res) {
            Session::flash('info', trans('smarticops::permissions.info.deleted', ['code' => $permission->code]));
        } else {
            Session::flash('info', trans('smarticops::permissions.error.deleting'));
        }
        
        return redirect('/permissions');
    }
    
}

