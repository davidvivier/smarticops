{{--

Simple Blade template to display errors.
Include this in a view with @include('errors.show')

--}}

@if (count($errors) > 0)
    <div>
        @foreach ($errors->all() as $error) 
            <div>
                {{ $error }}
            </div>
        
        @endforeach
    </div>
@endif
