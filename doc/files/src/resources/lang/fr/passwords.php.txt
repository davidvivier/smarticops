<?php
//fr
return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Le mot de passe doit contenir au moins '.Config::get('smarticops.password.min',8).' caract&egrave;res, et les mots de passe doivent correspondre.',
    'reset' => 'Votre mot de passe a &eacute;t&eacute; r&eacute;initialis&eacute; !',
    'sent' => 'Nous vous avons envoy&eacute; le lien de r&eacute;initialisation du mot de passe ! V&eacute;rifiez votre boite de réception et vos courriers ind&eacute;sirables.',
    'token' => 'Ce code de r&eacute;initialisation est invalide.',
    'user' => "Nous n'avons trouv&eacute; aucun utilisateur avec ce courriel.",

    'email.title'   => 'R&eacute;initialiser le mot de passe',
    'email.send'    => 'Envoyer le lien de r&eacute;initialisation',
    
    'email.subject' => 'Réinitialisation de votre mot de passe',
    
    'reset.title'   => 'R&eacute;initialiser le mot de passe',
    'reset.password' => 'Mot de passe',
    'reset.password_confirm' => 'Confirmez le mot de passe',
    'reset.button'  => 'R&eacute;initialiser le mot de passe',
];

