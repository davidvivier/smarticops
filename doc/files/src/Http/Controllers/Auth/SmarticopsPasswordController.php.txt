<?php
/**
 * Copyright 2016 David Vivier on behalf of Smartic.ca
 * 
 * This file is part of the Smarticops Package
 * 
 * License : MIT
 * 
 * @author David Vivier
 */

namespace  Dvivier\Smarticops\Http\Controllers\Auth;

use  Dvivier\Smarticops\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\ResetsPasswords;
use Dvivier\Smarticops\SmarticopsResetsPasswords;


/**
 *  A controller to handle password reset requests
 * 
 * @see Dvivier\Smarticops\SmarticopsResetsPasswords SmarticopsResetsPasswords
 */
class SmarticopsPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use SmarticopsResetsPasswords;
    
    protected $redirectTo = '/home';
    
    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //var_dump('SmarticopsPasswordController');
        
        // override views : use smarticops views instead of Laravel's views
        $this->linkRequestView = 'smarticops::auth.passwords.email';
        $this->resetView = 'smarticops::auth.passwords.reset';
        //var_dump($this->linkRequestView);
        $this->middleware('guest');
    }
    
    
    
}

