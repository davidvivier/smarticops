# Changelog

All notable changes to `Smarticops` will be documented in this file.  
This package follows the Semantic Versioning 2.0.0 specification described at [semver.org](http://semver.org/).

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## 0.1.0 - 2016-05-10

### Added
- Administration Panel
- SuperAdmin
- Users, Roles, Permissions
- Tests for Users, Roles, Permissions (Models and Controllers)
- Authentication
- Activity Logging (CRUD, authentication)
- Documentation
- Languages : English, French
- README.md, CHANGELOG.md, CONDUCT.md, LICENSE.md
